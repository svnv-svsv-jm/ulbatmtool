import pytest
from loguru import logger
import typing as ty
import sys

import numpy as np
import torch

from ulbtool import LPCA


def test_lpca(wine_dataset: ty.Tuple[np.ndarray, np.ndarray]) -> None:
    """Test LPCA on Wine data."""
    data, _ = wine_dataset
    X = torch.from_numpy(data)
    logger.info(f"X: {X.size()}")
    model = LPCA(n_clusters=5, q=3)
    model.fit(X)
    logger.info(model)


if __name__ == "__main__":
    logger.remove()
    logger.add(sys.stderr, level="TRACE")
    pytest.main([__file__, "-x", "-s", "--pylint"])

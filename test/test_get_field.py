import pytest
from loguru import logger
import sys

import numpy as np
from ulbtool.snapops import get_field


def test_get_field() -> None:
    """Test get_field()."""
    mesh = np.zeros((6, 1), dtype=float)
    variables = ["A", "B", "C"]
    X = np.random.rand(2, len(variables) * mesh.shape[0])
    field = get_field(choice=0, X=X, mesh=mesh, variables=variables)
    logger.info(field.shape)


if __name__ == "__main__":
    logger.remove()
    logger.add(sys.stderr, level="DEBUG")
    pytest.main([__file__, "-x", "-s", "--pylint", "--mypy"])

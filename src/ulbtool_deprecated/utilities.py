'''
MODULE: utilities.py

@Authors: 
    G. D'Alessio [1,2], G. Aversano [1], A. Parente[1]
    [1]: Université Libre de Bruxelles, Aero-Thermo-Mechanics Laboratory, Bruxelles, Belgium
    [2]: CRECK Modeling Lab, Department of Chemistry, Materials and Chemical Engineering, Politecnico di Milano

@Contacts:
    giuseppe.dalessio@ulb.ac.be

@Additional notes:
    This cose is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
    Please report any bug to: giuseppe.dalessio@ulb.ac.be

'''
from __future__ import print_function

__all__ = ["check_nparray", "check_method", "sanity_check", "TOL", "CEN_METHODS", "SCAL_METHODS", "INIT_METHODS", "auto_attr_check"]

import csv
import numpy as np
import pandas as pd


# -------------------
# Constants
# -------------------
TOL = 1E-10
CEN_METHODS = ['mean', 'min']
SCAL_METHODS = ['auto', 'pareto', 'vast', 'range']
INIT_METHODS = ['random', 'kmeans', 'uniform']



# -------------------
# Decorators
# -------------------
def getter_setter_gen(name, type_):
    def getter(self):
        return getattr(self, "__" + name)
    def setter(self, value):
        if not isinstance(value, type_):
            raise TypeError("%s attribute must be set to an instance of %s" % (name, type_))
        setattr(self, "__" + name, value)
    return property(getter, setter)


def auto_attr_check(cls):
    new_dct = {}
    for key, value in cls.__dict__.items():
        if isinstance(value, type):
            value = getter_setter_gen(key, value)
        new_dct[key] = value
    # Creates a new class, using the modified dictionary as the class dict:
    return type(cls)(cls.__name__, cls.__bases__, new_dct)



# -------------------
# Functions
# -------------------
def check_nparray(X):
    if not isinstance(X, np.ndarray):
        try:
            X = np.array(X)
        except:
            raise Exception("Unsupported variable type for input X.")
    return X


def check_method(method, listOfMethods):
    if not isinstance(method, (int, str)):
        raise Exception("Provide a valid method.")
    if isinstance(method, str) and str.lower(method) not in listOfMethods:
        raise Exception("Unsupported method. Supported values: {}".format(listOfMethods))
    elif isinstance(method, str) and str.lower(method) in listOfMethods:
        return method
    elif isinstance(method, int):
        if method > len(listOfMethods):
            raise Exception("Invalid method.")
        else:
            return listOfMethods[method]


def sanity_check(X, method, listOfMethods):
    # Inputs
    X = check_nparray(X)
    # Sanity checks
    method = check_method(method, listOfMethods)
    return X, method



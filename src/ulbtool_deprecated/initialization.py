'''
MODULE: initialization.py

@Authors: 
    G. D'Alessio [1,2], G. Aversano [1], A. Parente[1]
    [1]: Université Libre de Bruxelles, Aero-Thermo-Mechanics Laboratory, Bruxelles, Belgium
    [2]: CRECK Modeling Lab, Department of Chemistry, Materials and Chemical Engineering, Politecnico di Milano

@Contacts:
    giuseppe.dalessio@ulb.ac.be

@Additional notes:
    This cose is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
    Please report any bug to: giuseppe.dalessio@ulb.ac.be

'''
from __future__ import print_function

__all__ = ["initialize_clusters"]

import numpy as np
from sklearn.cluster import KMeans
from .utilities import *


# -----------------
# Functions
# -----------------
def initialize_clusters(X=1, k=1, method="random"):
    listOfMethods = INIT_METHODS
    method = check_method(method, listOfMethods)
    # Inputs
    if isinstance(X, int):
        n_obs = X # user inputted a number of observations
    elif isinstance(X, (np.ndarray)):
        n_obs = X.shape[0] # user inputted a matrix
    else:
        raise Exception("Usupported type for variable X.")
    # Main
    if method.lower() == listOfMethods[0]:
        idx = np.random.random_integers(low=1, high=k, size=n_obs)
    elif method.lower() == listOfMethods[1]:
        kmeans = KMeans(n_clusters=k, random_state=0).fit(X)
        idx = kmeans.labels_
    elif method.lower() == listOfMethods[2]:
        idx = np.linspace(start=1, stop=k, num=n_obs, dtype=int)
    return idx


# -----------------
# Main
# -----------------
if __name__ == '__main__':
    print("Testing this module...")
    idx = initialize_clusters(X=np.random.rand(50, 5), k=3, method="random")
    idx = initialize_clusters(X=np.random.rand(50, 5), k=3, method="kmeans")
    print("Test ok...")


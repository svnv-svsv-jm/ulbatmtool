'''
MODULE: decorators.py

@Authors: 
    G. D'Alessio [1,2], G. Aversano [1], A. Parente[1]
    [1]: Université Libre de Bruxelles, Aero-Thermo-Mechanics Laboratory, Bruxelles, Belgium
    [2]: CRECK Modeling Lab, Department of Chemistry, Materials and Chemical Engineering, Politecnico di Milano

@Contacts:
    giuseppe.dalessio@ulb.ac.be

@Additional notes:
    This cose is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
    Please report any bug to: giuseppe.dalessio@ulb.ac.be

'''
from __future__ import print_function

__all__ = ["PLUGINS", "timer", "debug", "register", "count_calls", "singleton", "cache", "set_unit", "accepts", "repeat", "CountCalls", "RunTime"]

import functools
import time
import numpy as np


# -------------------
# Constants
# -------------------
PLUGINS = dict()  # Dictionary used by @register to store plugins


# -------------------
# Decorator functions
# -------------------
def timer(func):
    """Prints the runtime of the decorated function"""
    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        start_time = time.perf_counter()
        value = func(*args, **kwargs)
        end_time = time.perf_counter()
        run_time = end_time - start_time
        print(f"Finished {func.__name__!r} in {run_time:.4f} secs")
        return value
    return wrapper_timer


def debug(func):
    """Prints the function signature and return value"""
    @functools.wraps(func)
    def wrapper_debug(*args, **kwargs):
        args_repr = [repr(a) for a in args]
        kwargs_repr = [f"{k}={v!r}" for k, v in kwargs.items()]
        signature = ", ".join(args_repr + kwargs_repr)
        print(f"Calling {func.__name__}({signature})")
        value = func(*args, **kwargs)
        print(f"{func.__name__!r} returned {value!r}")
        return value
    return wrapper_debug


def register(func):
    """Register a function as a plug-in"""
    PLUGINS[func.__name__] = func
    return func


def count_calls(func):
    """Count the number of calls made to the decorated function"""
    @functools.wraps(func)
    def wrapper_count_calls(*args, **kwargs):
        wrapper_count_calls.num_calls += 1
        print(f"Call {wrapper_count_calls.num_calls} of {func.__name__!r}")
        return func(*args, **kwargs)
    wrapper_count_calls.num_calls = 0
    return wrapper_count_calls


def singleton(cls):
    """Make a class a Singleton class (only one instance)"""
    @functools.wraps(cls)
    def wrapper_singleton(*args, **kwargs):
        if not wrapper_singleton.instance:
            wrapper_singleton.instance = cls(*args, **kwargs)
        return wrapper_singleton.instance
    wrapper_singleton.instance = None
    return wrapper_singleton


def cache(func): #rather use @functools.lru_cache(maxsize=4)
    """
    Keeps a cache of previous function calls. Consider using @functools.lru_cache(maxsize=N) instead of this decorator.
    """
    @functools.wraps(func)
    def wrapper_cache(*args, **kwargs):
        cache_key = args + tuple(kwargs.items())
        if cache_key not in wrapper_cache.cache:
            wrapper_cache.cache[cache_key] = func(*args, **kwargs)
        return wrapper_cache.cache[cache_key]
    wrapper_cache.cache = dict()
    return wrapper_cache


def set_unit(unit):
    """Register a unit on a function"""
    def decorator_set_unit(func):
        func.unit = unit
        return func
    return decorator_set_unit


def accepts(*types):
    """Checks argument types.
    Usage (example):
        @accepts(int, (int,float)) # arg2 can be either int or float
        def func(arg1, arg2):
            return arg1 * arg2

        func(3, 2) # -> 6
        func('3', 2) # -> AssertionError: arg '3' must be of <type 'int'>
    """
    def decorator(f):
        assert len(types) == f.__code__.co_argcount
        @functools.wraps(f)
        def wrapper(*args, **kwds):
            for (a, t) in zip(args, types):
                assert isinstance(a, t), "arg %r must be of type <%s>" % (a,t)
            return f(*args, **kwds)
        wrapper.__name__ = f.__name__
        return wrapper
    return decorator


def force_casting(*types):
    """Forces inputs to be of a certain type. Do not use yet."""
    def decorator(f):
        assert len(types) == f.__code__.co_argcount
        @functools.wraps(f)
        def wrapper(*args, **kwargs):
            cargs = []
            for (a, t) in zip(args, types):
                if not isinstance(a, t):
                    pass
                    # a = np.asarray(a) # this line needs to be replaced, no solution found yet
                cargs.append(a)
            cargs = tuple(cargs)
            return func(*cargs, **kwargs)
        wrapper.__name__ = f.__name__
        return wrapper
    return decorator


def numpy_cast(func):
    """Checks input is a numpy array."""
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        cargs = []
        for a in args:
            if not isinstance(a, np.ndarray):
                a = np.asarray(a)
            cargs.append(a)
        cargs = tuple(cargs)
        return func(*cargs, **kwargs)
    return wrapper


def repeat(_func=None, *, num_times=2):
    """Runs the decorated function the given number of times"""
    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            for _ in range(num_times):
                value = func(*args, **kwargs)
            return value
        return wrapper
    if _func is None:
        return decorator
    else:
        return decorator(_func)



# -------------------
# Decorator classes
# -------------------
class CountCalls:
    """Counts the number of calls made to the decorated function"""
    def __init__(self, func):
        functools.update_wrapper(self, func)
        self.func = func
        self.num_calls = 0
    def __call__(self, *args, **kwargs):
        self.num_calls += 1
        print(f"Call {self.num_calls} of {self.func.__name__!r}")
        return self.func(*args, **kwargs)


class RunTime(object):
    """Evaluates the runtime of the decorated function"""
    def __init__(self, func):
        functools.update_wrapper(self, func)
        self.func = func
        self._runtime = 0
    def __call__(self, *args, **kwargs):
        start_time = time.perf_counter()
        value = self.func(*args, **kwargs)
        end_time = time.perf_counter()
        self.runtime = end_time - start_time
        return value
    def __get__(self, obj, objtype):
        return functools.partial(self.__call__, obj)
    @property
    def runtime(self):
        return self._runtime
    @runtime.setter
    def runtime(self, value):
        self._runtime = value




# ------------
# Main
# ------------
if __name__ == '__main__':
    print("testing...")
    # testing check_nparray
    @numpy_cast
    def func(value):
        return value
    val = func([1, 1, 1, 1])
    print(type(val))




"""
MODULE: operations.py

@Authors:
    G. D'Alessio [1,2], G. Aversano [1], A. Parente[1]
    [1]: Université Libre de Bruxelles, Aero-Thermo-Mechanics Laboratory, Bruxelles, Belgium
    [2]: CRECK Modeling Lab, Department of Chemistry, Materials and Chemical Engineering, Politecnico di Milano

@Contacts:
    giuseppe.dalessio@ulb.ac.be

@Additional notes:
    This cose is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    Please report any bug to: giuseppe.dalessio@ulb.ac.be

"""

__all__ = [
    "get_cluster",
    "get_centroids",
    "fitPCA",
    "fitLPCA",
    "PHC_index",
    "NRMSE",
    "evaluate_clustering_DB",
    "merge_clusters",
    "explained_variance",
    "cluster_dist",
]

import sys, os
import numpy as np
import numpy.matlib as nmb
import multiprocessing
from joblib import Parallel, delayed
from scipy.spatial.distance import euclidean, cdist

try:
    from .utilities import *
    from .decorators import *
except ImportError as import_error:
    print("Import error: {}.\nManually importing...".format(import_error))
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
    from ulbtool.utilities import *
    from ulbtool.decorators import *


# -----------------
# Constants
# -----------------
CORR = ["false", "off", "mean", "min", "max", "std", "phc"]


# -----------------
# Functions
# -----------------
@accepts(np.ndarray, np.ndarray)
def NRMSE(X_true, X_pred):
    n_obs, n_var = X_true.shape
    NRMSE = [None] * n_var
    for ii in range(0, n_var):
        NRMSE[ii] = np.sqrt(np.mean((X_true[:, ii] - X_pred[:, ii]) ** 2)) / np.sqrt(
            np.mean(X_true[:, ii] ** 2)
        )
    return NRMSE


@accepts(np.ndarray, np.ndarray, int)
def get_cluster(X, idx, index):
    return np.array(X[idx == index])


@accepts(np.ndarray, (np.ndarray, None))
def get_centroids(X, idx=None):
    if idx is None:
        idx = np.ones(X.shape[0], dtype=int)
    centroid = []
    for index in np.unique(idx):
        centroid.append(np.mean(X[idx == index], axis=0))
    return np.array(centroid)


@accepts(np.ndarray, bool)
def fitPCA(X, Sir=False):
    """
    Uses SVD of X to perform PCA. Data are neither centered nor scaled.
    INPUT:
    - X (observations, variables): data matrix
    - Sir (bool): if TRUE, will perform Sirovic's POD
    OUTPUT:
    - A, L, Z: modes, eigenvalues and scores
    - C: Sirovic's temporal eigenvectors/weights
    """
    n_obs = X.shape[0]
    Z, S, Vh = np.linalg.svd(X, full_matrices=True)
    A = Vh.transpose()  # PCA modes
    L = np.power(S, 2) / (n_obs - 1)  # Eigenvalues
    Z = np.matmul(X, A)  # PCA scores
    C = None
    if Sir:
        S = np.matmul(X, X.transpose())
        _, C = np.linalg.LA.eig(S)
    return A, L, Z, C


def cluster_dist(X, centroid, A, method="false"):
    """
    Computes the distance of each observation in "X" from a manifold of center "centroid" and hyper-plane identified by "A".
    INPUT
    X (matrix = obs x vars): observations
    centroid (array = vars): coordinates of the manifold's center
    A (matrix = vars x q): set of q directions that identify an hyper-plane
    OUTPUT
    sq_rec_err (array): each entry is the distance of the i-th row of X from the manifold identified by "A" and centered in "centroid".
    """
    # Constants
    method = check_method(method, CORR)
    # Main
    rows, cols = np.shape(X)
    C_mat = nmb.repmat(centroid, rows, 1)
    rec_err_os = (X - C_mat) - (X - C_mat) @ A @ np.transpose(A)
    sq_rec_oss = np.power(rec_err_os, 2)
    sq_rec_err = sq_rec_oss.sum(axis=1)
    if method.lower() != "false" and method.lower() != "off":
        tmp = np.var((X - C_mat) @ A, axis=0)
        if method.lower() == "phc":
            pass
        elif method.lower() == "mean":
            correction = np.mean(tmp)
        elif method.lower() == "max":
            correction = np.max(tmp)
        elif method.lower() == "min":
            correction = np.min(tmp)
        elif method.lower() == "std":
            correction = np.std(tmp)
        sq_rec_err = np.multiply(sq_rec_err, correction)
    return sq_rec_err


@accepts(int, np.ndarray, np.ndarray, bool, (str, int))
def lpca_onecluster(ii, X, idx, Sir=False, method="false"):
    """
    This is the body of a parallel job that scales on "ii": each process works with a different value of "ii".
    """
    # Constants
    method = check_method(method, CORR)
    # Main
    rows, cols = np.shape(X)
    centroids = get_centroids(X=X, idx=idx)
    cluster = get_cluster(X=X, idx=idx, index=ii)
    A, L, Z, C = fitPCA(X=cluster, Sir=Sir)
    sq_rec_err = cluster_dist(X=X, centroid=centroids[ii], A=A, method=method)
    return A, L, Z, sq_rec_err, C


@accepts(np.ndarray, np.ndarray, bool, (str, int))
def fitLPCA(X, idx, Sir=False, method="false"):
    """
    Performs PCA (SVD) in each cluster.
    """
    # Constants
    method = check_method(method, CORR)
    # Initialize
    rows, cols = np.shape(X)
    n_clusters = len(np.unique(idx))
    centroids = get_centroids(X=X, idx=idx)
    sq_rec_err = np.zeros((rows, n_clusters), dtype=float)
    A = [None] * n_clusters
    L = [None] * n_clusters
    Z = [None] * n_clusters
    C = [None] * n_clusters
    # Perform PCA in each cluster (parallel looks slower: perhas just overhead time to get workers to turn on)
    num_cores = multiprocessing.cpu_count()
    res = Parallel(n_jobs=num_cores)(
        delayed(lpca_onecluster)(ii, X, idx, Sir, method) for ii in range(0, n_clusters)
    )
    for ii in range(0, n_clusters):
        A[ii], L[ii], Z[ii], sq_rec_err[:, ii], C[ii] = res[
            ii
        ]  # lpca_onecluster(ii=ii, X=X, idx=idx, Sir=Sir)
    if method.lower() == "phc":
        PHC_coefficients, PHC_std = PHC_index(X, idx)
        sq_rec_err = np.add(sq_rec_err, PHC_coefficients)
    return A, L, Z, centroids, sq_rec_err, C


@accepts(np.ndarray, bool)
def merge_clusters(idx, verbose=False):
    """
    Remove a cluster if it is empty, or not statistically meaningful.
    """
    clust = np.unique(idx)
    for jj, cc in enumerate(clust):
        numel = len(idx[(idx == cc)])
        if numel < 2:
            pos = idx == cc
            try:
                idx[pos] = clust[jj - 1]
            except:
                idx[pos] = clust[jj + 1]
            if verbose:
                print("WARNING:")
                print("\tAn empty cluster was found:")
                print(
                    "\tThe number of cluster was lowered to ensure statistically meaningful results."
                )
                k = len(np.unique(idx))
                print("\tThe current number of clusters is equal to: {}".format(k))
            break
    return idx


@accepts(np.ndarray, np.ndarray)
def PHC_index(X, idx):
    """
    Computes the PHC (Physical Homogeneity of the Cluster) index.
    For many applications, more than a pure mathematical tool to assess the quality of the clustering solution, such as the Silhouette Coefficient, a measure of the variables variation is more suitable. This coefficient assess the quality of the clustering solution measuring the variables variation in each cluster. The more the PHC approaches to zero, the better the clustering.
    INPUT:
    X = UNCENTERED/UNSCALED data matrix -- dim: (observations x variables)
    idx = class membership vector -- dim: (obs x 1)
    - Output:
    PHC_coeff = vector with the PHC scores for each cluster -- dim: (number_of_cluster)
    PHC_deviations = vector with the mean standard deviation of each cluster
    """
    clust = np.unique(idx)
    PHC_coeff = [None] * len(clust)
    PHC_deviations = [None] * len(clust)
    for ii, k in enumerate(clust):
        cluster_ = get_cluster(X, idx, k)
        maxima = np.max(cluster_, axis=0)
        minima = np.min(cluster_, axis=0)
        media = np.mean(cluster_, axis=0)
        dev = np.std(cluster_, axis=0)
        PHC_coeff[ii] = np.mean((maxima - minima) / (media + TOL))
        PHC_deviations[ii] = np.mean(dev)
    return PHC_coeff, PHC_deviations


@accepts(np.ndarray, int)
def explained_variance(X, n_eigs):
    """
    Assess the variance explained by the first 'n_eigs' retained Principal Components. This is important to know if the percentage of explained variance is enough, or additional PCs must be retained.
    INPUT:
    X = CENTERED/SCALED data matrix -- dim: (observations x variables)
    n_eigs = number of components to retain -- dim: (scalar, int)
    OUTPUT:
    explained: percentage of explained variance -- dim: (scalar)
    """
    A, eigens, Z, _ = fitPCA(X)
    explained_variance = np.cumsum(eigens) / sum(eigens)
    explained = explained_variance[n_eigs]
    return explained


@accepts(np.ndarray, np.ndarray)
def evaluate_clustering_DB(X, idx):
    """
    Davies-Bouldin index a coefficient to evaluate the goodness of a clustering solution. The more it approaches to zero, the better the clustering solution is.
    """
    # Initialize matrix and other quantitites
    k = len(np.unique(idx))
    centroids = get_centroids(X, idx)
    S = [None] * k
    M = np.zeros((k, k), dtype=float)
    # For each cluster, compute the mean distance between the points and their centroids
    for ii in range(0, k):
        cluster_ = get_cluster(X, idx, ii)
        S[ii] = np.mean(cdist(cluster_, centroids[ii].reshape((1, -1))))
    # Compute the distance between once centroid and all the others:
    for ii in range(0, k):
        for jj in range(0, k):
            if ii != jj:
                M[ii, jj] = euclidean(centroids[ii], centroids[jj])
            else:
                M[ii, jj] = 1
    R = np.empty((k, k), dtype=float)
    # Compute the R_ij coefficient for each couple of clusters, using the
    # two coefficients S_ij and M_ij
    for ii in range(0, k):
        for jj in range(0, k):
            if ii != jj:
                R[ii, jj] = (S[ii] + S[jj]) / M[ii, jj] + TOL
            else:
                R[ii, jj] = 0
    D = [None] * k
    # Compute the Davies-Bouldin index as the mean of the maximum R_ij value
    for ii in range(0, k):
        D_[ii] = np.max(R[ii], axis=0)
    DB = np.mean(D)
    return DB


# ------------
# Main
# ------------
if __name__ == "__main__":
    print("testing...")


# --- See this for aliasing
# import copy
# def is_sorted(t):
#     a = copy.copy(t) # could also do: a = t[:]
#     a.sort()
#     return a == t

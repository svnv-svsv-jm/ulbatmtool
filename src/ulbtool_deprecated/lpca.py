# -*- coding: utf-8 -*-
"""
@Contacts:
    giuseppe.dalessio@ulb.ac.be

@Brief: 
    Clustering via Local Principal Component Analysis.

@Details: 
    The iterative Local Principal Component Analysis algorithm is based on the following steps:
    0. Preprocessing: The training matrix X is centered and scaled. Four scaling are available,
    AUTO, VAST, PARETO, RANGE - Two centering are available, MEAN and MIN;
    1. Initialization: The cluster centroids are initializated: a random allocation (RANDOM)
    or a previous clustering solution (KMEANS) can be chosen to compute the centroids initial values; 
    2. Partition: Each observation is assigned to a cluster k such that the local reconstruction
    error is minimized;
    3. PCA: The Principal Component Analysis is performed in each of the clusters found
    in the previous step. A new set of centroids is computed after the new partitioning
    step, their coordinates are calculated as the mean of all the observations in each
    cluster;
    4. Iteration: All the previous steps are iterated until convergence is reached. The convergence
    criterion is that the variation of the global mean reconstruction error between two consecutive
    iterations must be below a fixed threshold.

@Cite:
    - Local algorithm for dimensionality reduction:
    [a] Kambhatla, Nandakishore, and Todd K. Leen. "Dimension reduction by local principal component analysis.", Neural computation 9.7 (1997): 1493-1516.

    - Clustering applications:
    [b] D'Alessio, Giuseppe, et al. "Adaptive chemistry via pre-partitioning of composition space and mechanism reduction.", Combustion and Flame 211 (2020): 68-82.

    - Data analysis applications:
    [c] Parente, Alessandro, et al. "Investigation of the MILD combustion regime via principal component analysis." Proceedings of the Combustion Institute 33.2 (2011): 3333-3341.
    [d] D'Alessio, Giuseppe, et al. "Analysis of turbulent reacting jets via Principal Component Analysis", Data Analysis in Direct Numerical Simulation of Turbulent Combustion, Springer (2020).
    [e] Bellemans, Aurélie, et al. "Feature extraction and reduced-order modelling of nitrogen plasma models using principal component analysis." Computers & chemical engineering 115 (2018): 504-514.

    - Preprocessing effects on PCA:
    [f] Parente, Alessandro, and James C. Sutherland. "Principal component analysis of turbulent combustion data: Data pre-processing and manifold sensitivity." Combustion and flame 160.2 (2013): 340-350.

    - Model order reduction:
    [g] Parente, Alessandro, et al. "Identification of low-dimensional manifolds in turbulent flames." Proceedings of the Combustion Institute. 2009 Jan 1;32(1):1579-86.
    [h] Aversano, Gianmarco, et al. "Application of reduced-order models based on PCA & Kriging for the development of digital twins of reacting flow applications." Computers & chemical engineering 121 (2019): 422-441.

@Additional notes:
    This cose is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
    Please report any bug to: giuseppe.dalessio@ulb.ac.be
"""
from __future__ import print_function

__all__ = ["Lpca"]

import matplotlib.pyplot as plt
import numpy as np
import time

from .initialization import *
from .operations import *
from .preprocessing import *
from .utilities import *
from .classes import *
from .decorators import *
from .snapops import *


# -----------------
# Classes
# -----------------
class Lpca(Data, Model):
    def __init__(self):
        # --- Attributes ---
        # super().__init__()
        Data.__init__(self)
        Model.__init__(self)
        # Inputs
        self._k = 1
        # Outputs
        self._idx = np.empty(shape=1, dtype=int)
        self.centroids = None  # (k * variables)
        self._kout = 1
        # One PCA solution per cluster
        self.A = None
        self.L = None
        self.Z = None
        self.C = None  # Sirovic's modes
        # Algorithm settings
        self.Sir = False
        self.lpca_method = "false"
        self.residuals = None

    # --- Properties ---
    # vector assignments
    @property
    def idx(self):
        return self._idx

    @idx.setter
    @accepts(object, np.ndarray)
    def idx(self, value):
        self._idx = value

    # number of clusters
    @property
    def k(self):
        return self._k

    @k.setter
    @accepts(object, int)
    def k(self, value):
        self._k = value

    # output number of clusters
    @property
    def kout(self):
        return self._kout

    @kout.setter
    @accepts(object, int)
    def kout(self, value):
        self._kout = value

    # --- Methods ---
    def plot_residuals(
        self,
        font_size=18,
        color="b",
        marker="s",
        linestyle="-",
        linewidth=2,
        markersize=4,
        markerfacecolor="b",
    ):
        """
        Plots residuals.
        """
        matplotlib.rcParams.update({"font.size": font_size, "text.usetex": True})
        itr = np.linspace(1, len(self.residuals), len(self.residuals))
        fig = plt.figure()
        axes = fig.add_axes([0.15, 0.15, 0.7, 0.7], frameon=True)
        axes.plot(
            itr,
            self.residuals,
            color=color,
            marker=marker,
            linestyle=linestyle,
            linewidth=linewidth,
            markersize=markersize,
            markerfacecolor=markerfacecolor,
        )
        axes.set_xlabel("Iterations [-]")
        axes.set_ylabel("Reconstruction error [-]")
        axes.set_title("Local PCA")
        plt.show()

    def fit(self):
        """
        Performs Local PCA.
        """
        self.verboseprint("Fitting Local PCA model with {} clusters...".format(self.k))
        # Initialization
        convergence = False
        iteration = 0
        eps_rec = 1.0
        rows, cols = np.shape(self.X)
        residuals = np.array(0)
        start_time = time.perf_counter()
        # Data pre-processing
        self.verboseprint("Centering and scaling the training matrix..")
        # self.mu = center(X=self.X, method=self.cen_method)
        # self.sig = scale(X=self.X, method=self.scal_method)
        # self.X0 = center_scale(X=self.X, mu=self.mu, sig=self.sig)
        self.preprocess()
        # Initialize solution
        idx = initialize_clusters(X=self.X, k=self.k, method=self.init_method)
        # Iterate
        while iteration < self.iter_max and not convergence:
            A, L, Z, centroids, sq_rec_err, C = fitLPCA(
                X=self.X0, idx=idx, Sir=self.Sir, method=self.lpca_method
            )
            # Update idx
            idx = np.argmin(sq_rec_err, axis=1)
            # Evaluate recovered variance
            rec_err_min = np.min(sq_rec_err, axis=1)
            eps_rec_new = np.mean(rec_err_min, axis=0)
            eps_rec_var = np.abs((eps_rec_new - eps_rec) / (eps_rec_new + TOL))
            eps_rec = eps_rec_new
            # Print info
            self.verboseprint("- Iteration number: {}".format(iteration + 1))
            self.verboseprint("\tReconstruction error: {}".format(eps_rec_new))
            self.verboseprint("\tReconstruction error variance: {}".format(eps_rec_var))
            # Check convergence
            if eps_rec_var <= self.conv_tol:
                convergence = True
            else:
                residuals = np.append(residuals, eps_rec_new)
            # Update counter
            iteration += 1
        # Final steps
        end_time = time.perf_counter()
        self.runtime = end_time - start_time
        self.verboseprint("Convergence reached in {} iterations.".format(iteration))
        self.A = A
        self.L = L
        self.Z = Z
        self.C = C
        self.kout = len(A)
        self.idx = idx
        self.centroids = centroids
        self.residuals = residuals
        self.verboseprint(f"Local PCA model initialized!")

    def classify(self, X_new):
        """
        Classifies new observations and assigns them a cluster.
        """
        # Check consistency
        rows, cols = np.shape(X_new)
        if cols != np.shape(self.X)[1]:
            raise Exception("Number of columns must match.")
        # Pre-process new data
        self.verboseprint("Classifying new observations...")
        X0_new = center_scale(X=X_new, mu=self.mu, sig=self.sig)
        n_clusters = len(self.A)
        sq_rec_err = np.zeros((rows, n_clusters), dtype=float)
        for ii in range(0, n_clusters):
            sq_rec_err[:, ii] = cluster_dist(
                X0_new, self.centroids[ii], self.A[ii], method="false"
            )
        # Assign the label
        idx = np.argmin(sq_rec_err, axis=1)
        self.verboseprint("New observations classified.")
        return idx

    def recover(self):
        """
        Reconstruct the original matrix from the reduced PCA-manifold.
        - Output:
        X_rec = uncentered and unscaled reconstructed matrix with PCA modes -- dim: (observations x variables)
        """
        X_rec = self.X * 0
        clust = np.unique(self.idx)
        for ii in range(0, len(self.A)):
            I = self.idx == clust[ii]
            X_rec[I, :] = self.Z[ii] @ np.transpose(self.A[ii]) + self.centroids[ii]
        X_rec = X_rec * (self.sig + TOL)
        X_rec = X_rec + self.mu
        return X_rec

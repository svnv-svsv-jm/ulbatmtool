'''
MODULE: preprocessing.py

@Authors: 
    G. D'Alessio [1,2], G. Aversano [1], A. Parente[1]
    [1]: Université Libre de Bruxelles, Aero-Thermo-Mechanics Laboratory, Bruxelles, Belgium
    [2]: CRECK Modeling Lab, Department of Chemistry, Materials and Chemical Engineering, Politecnico di Milano

@Contacts:
    giuseppe.dalessio@ulb.ac.be

@Additional notes:
    This cose is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
    Please report any bug to: giuseppe.dalessio@ulb.ac.be

'''

__all__ = ["center", "scale", "center_scale"]

import sys, os
import numpy as np

try:
    from .utilities import *
    from .decorators import *
except ImportError as import_error:
    print("Import error: {}.\nManually importing...".format(import_error))
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
    from ulbtool.utilities import *
    from ulbtool.decorators import *



# -----------------
# Functions
# -----------------
@accepts(np.ndarray, (int,str), bool)
def center(X, method, cs=False):
    '''
    Computes the centering factors (the mean/min value [mu]) of each variable of all data-set observations and (eventually) returns the centered matrix.
    INPUT:
    X = original data matrix -- dim: (observations x variables);
    method = "string", it is the method which has to be used. Two choices are available: MEAN or MIN
    cs = boolean, choose if the funciton must return the centered matrix (optional)
    OUTPUT:
    mu = centering factor for the data matrix X
    X0 = centered data matrix (optional)
    '''
    listOfMethods = CEN_METHODS
    method = check_method(method, listOfMethods)
    # Main
    if method.lower() == listOfMethods[0]:
        mu = np.mean(X, axis = 0)
    elif method.lower() == listOfMethods[1]:
        mu = np.min(X, axis = 0)
    # return
    if cs:
        X0 = X - mu
        return mu, X0
    else:
        return mu


@accepts(np.ndarray, (int,str), bool)
def scale(X, method, cs=False):
    '''
    Computes the scaling factors of each variable of all data-set observations and (eventually) returns the centered matrix.
    INPUT:
    X = original data matrix -- dim: (observations x variables);
    method = "string", it is the method which has to be used. Two choices are available: MEAN or MIN
    cs = boolean, choose if the funciton must return the centered matrix (optional)
    OUTPUT:
    sig = scaling factor for the data matrix X
    X0 = scaled data matrix (optional)
    '''
    listOfMethods = SCAL_METHODS
    method = check_method(method, listOfMethods)
    # Main
    if method.lower() == listOfMethods[0]:
        sig = np.std(X, axis = 0)
    elif method.lower() == listOfMethods[1]:
        sig = np.sqrt(np.std(X, axis = 0))
    elif method.lower() == listOfMethods[2]:
        variances = np.var(X, axis = 0)
        means = np.mean(X, axis = 0)
        sig = variances / means
    elif method.lower() == listOfMethods[3]:
        maxima = np.max(X, axis = 0)
        minima = np.min(X, axis = 0)
        sig = maxima - minima
    # return
    if cs:
        X0 = X / (sig + TOL)
        return sig, X0
    else:
        return sig


@accepts(np.ndarray, (np.ndarray,int,float), (np.ndarray,int,float))
def center_scale(X, mu, sig):
    '''
    Center and scale a given multivariate data-set X. Centering consists of subtracting the mean/min value of each variable to all data-set observations. Scaling is achieved by dividing each variable by a given scaling factor. Therefore, the i-th observation of the j-th variable, x_{i,j} can be centered and scaled by means of:

    \tilde{x_{i,j}} = (x_{i,j} - mu_{j}) / (sig_{j}),

    where mu_{j} and sig_{j} are the centering and scaling factor for the considered j-th variable, respectively.

    AUTO: the standard deviation of each variable is used as a scaling factor.
    PARETO: the squared root of the standard deviation is used as a scaling f.
    RANGE: the difference between the minimum and the maximum value is adopted as a scaling f.
    VAST: the ratio between the variance and the mean of each variable is used as a scaling f.
    '''
    # Main
    X0 = X - mu
    X0 = X0 / (sig + TOL)
    return X0



# ------------
# Main
# ------------
if __name__ == '__main__':
    print("testing...")
    

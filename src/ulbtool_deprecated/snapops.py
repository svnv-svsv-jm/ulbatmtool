"""
MODULE: snapops.py

@Authors:
    G. D'Alessio [1,2], G. Aversano [1], A. Parente[1]
    [1]: Université Libre de Bruxelles, Aero-Thermo-Mechanics Laboratory, Bruxelles, Belgium
    [2]: CRECK Modeling Lab, Department of Chemistry, Materials and Chemical Engineering, Politecnico di Milano

@Contacts:
    giuseppe.dalessio@ulb.ac.be

@Additional notes:
    This cose is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    Please report any bug to: giuseppe.dalessio@ulb.ac.be
"""

__all__ = ["get_field"]

import sys
import numpy as np
import numpy.matlib as nmb

from .utilities import *
from .decorators import *


# -----------------
# Functions
# -----------------
@accepts((str, int), np.ndarray, np.ndarray, list, bool)
def get_field(choice, X, mesh, variables, verbose=False):
    """
    Given a snapshot matrix, this function returns the field(s) of the indicated variable ("choice" input argument).
    INPUT:
    - X (observations, variables): data matrix, each row is a set of vectorized spatial fields.
    - choice (str or int): variable whose field you want to select.
    - mesh (points, coordinates): spatial mesh of the fields.
    - variables (list[str]): names of the variables.
    OUTPUT:
    - output: subset of the matrix X: same number of rows, only selected columns.
    """
    # Check consistency of X
    assert (
        X.shape[1] == len(variables) * mesh.shape[0]
    ), "Incosistent inputs: X.shape[1] = {}; len(variables) = {}; mesh.shape[0] = {}.".format(
        X.shape[1], len(variables), mesh.shape[0]
    )
    # Handle choice
    if isinstance(choice, str):
        if choice not in variables:
            raise Exception("Choice has to be in variables.")
        # Find position of the variable
        for ii in range(len(variables)):
            if variables[ii] == choice:
                pos = ii
                break
    elif isinstance(choice, int):
        if choice < 0 or choice > len(variables):
            raise Exception("Index out of bounds.")
        pos = choice  # position of the variable
        choice = variables[choice]
    # Get starting and ending column indeces
    if verbose:
        print("pos = {}".format(pos))
        print("mesh.shape[0] = {}".format(mesh.shape[0]))
    start = mesh.shape[0] * pos
    end = mesh.shape[0] * (pos + 1)  # - 1 (end index is not included)
    if verbose:
        print("start = {}".format(start))
        print("end = {}".format(end))
    # Do not go beyond the end
    if end > X.shape[1]:
        end = X.shape[1]
    # Return extracted fields
    field = X[:, start:end]
    assert (
        field.shape[1] == mesh.shape[0]
    ), "Bug encountered in this function: {}\tfield.shape[1] = {}\tmesh.shape[0] = {}.".format(
        sys._getframe().f_code.co_name, field.shape[1], mesh.shape[0]
    )
    if verbose:
        print(
            "field.shape[1] = {}\tmesh.shape[0] = {}.".format(
                field.shape[1], mesh.shape[0]
            )
        )
    return X[:, start:end]

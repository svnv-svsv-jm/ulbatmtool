"""
MODULE: classmod.py

@Authors: 
    G. D'Alessio [1,2], G. Aversano [1], A. Parente[1]
    [1]: Université Libre de Bruxelles, Aero-Thermo-Mechanics Laboratory, Bruxelles, Belgium
    [2]: CRECK Modeling Lab, Department of Chemistry, Materials and Chemical Engineering, Politecnico di Milano

@Contacts:
    giuseppe.dalessio@ulb.ac.be

@Additional notes:
    This cose is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
    Please report any bug to: giuseppe.dalessio@ulb.ac.be

"""
from __future__ import print_function

__all__ = ["Data", "Model"]

import numpy as np
from abc import ABC, abstractmethod


# -------------------
# Classes
# -------------------
class Data(object):
    """
    Simple class of a data-object. Supports common attributes such as pre-processed data 'X0', centering and scaling factors and criteria.
    """

    # --- Class attributes ---
    CEN_METHODS = ["mean", "min"]
    SCAL_METHODS = ["auto", "pareto", "vast", "range"]

    # --- Attributes of object ---
    def __init__(self):
        # Inputs
        super().__init__()  # imports constructor from superclass
        self._X = np.empty(shape=[1, 1], dtype=float)  # data
        self._is_spatial = False
        self._mesh = None
        self._labels = None
        self._params = None  # input parameters
        # Outputs
        self._X0 = np.empty(shape=[1, 1], dtype=float)  # processed data
        self.mu = 0  # global centering
        self.sig = 1.0  # global scaling
        # Config
        self._cen_method = Data.CEN_METHODS[0]
        self._scal_method = Data.SCAL_METHODS[0]

    # --- Properties ---
    @property
    def labels(self):
        return self._labels

    @labels.setter
    @accepts(object, list)
    def labels(self, value):
        self._labels = value

    @property
    def is_spatial(self):
        return self._is_spatial

    @is_spatial.setter
    @accepts(object, bool)
    def is_spatial(self, value):
        self._is_spatial = value

    @property
    def mesh(self):
        return self._mesh

    @mesh.setter
    @accepts(object, (np.ndarray, np.array))
    def mesh(self, value):
        self._mesh = value

    @property
    def params(self):
        return self._params

    @params.setter
    @accepts(object, (np.ndarray, np.array))
    def params(self, value):
        self._params = value

    @property
    def X(self):
        return self._X

    @X.setter
    @accepts(object, (np.ndarray, np.array, list, tuple))
    def X(self, value):
        if isinstance(value, list) or isinstance(value, tuple):
            value = np.asarray(value)
        self._X = value

    @property
    def X0(self):
        return self._X0

    @X0.setter
    @accepts(object, (np.ndarray, np.array, list, tuple))
    def X0(self, value):
        if isinstance(value, list) or isinstance(value, tuple):
            value = np.asarray(value)
        self._X0 = value

    @property
    def cen_method(self):
        return self._cen_method

    @cen_method.setter
    def cen_method(self, value):
        self._cen_method = check_method(value, CEN_METHODS)

    @property
    def scal_method(self):
        return self._scal_method

    @scal_method.setter
    def scal_method(self, value):
        self._scal_method = check_method(value, SCAL_METHODS)

    # --- Methods ---
    def get_field(self, choice):
        return get_field(choice=choice, X=self.X, mesh=self.mesh, variables=self.labels)

    def preprocess(self):
        self.mu = center(X=self.X, method=self.cen_method)
        self.sig = scale(X=self.X, method=self.scal_method)
        self.X0 = center_scale(X=self.X, mu=self.mu, sig=self.sig)

    def show_info(self):
        print("Data:")
        print(f"\t X{self.X.shape} of type {type(self.X)}")
        print(f"\t mesh{self.mesh.shape} of type {type(self.mesh)}")
        print(f"\t variables: {self.labels}")
        print(f"\t parameters: {self.params}")
        print(f"\t is_spatial: {self.is_spatial}")

    def get_field(self, choice, proc=False, verbose=False):
        if proc:
            if self.is_spatial:  # will look for a field among the columns
                field = get_field(
                    choice=choice,
                    X=self.X0,
                    mesh=self.mesh,
                    variables=self.labels,
                    verbose=verbose,
                )
            else:  # will look for a field among the rows
                field = get_field(
                    choice=choice,
                    X=np.transpose(self.X0),
                    mesh=self.mesh,
                    variables=self.labels,
                    verbose=verbose,
                )
        else:
            if self.is_spatial:  # will look for a field among the columns
                field = get_field(
                    choice=choice,
                    X=self.X,
                    mesh=self.mesh,
                    variables=self.labels,
                    verbose=verbose,
                )
            else:  # will look for a field among the rows
                field = get_field(
                    choice=choice,
                    X=np.transpose(self.X),
                    mesh=self.mesh,
                    variables=self.labels,
                    verbose=verbose,
                )
        return field


class Model(ABC):
    """
    Simple abstract class of a model-object, forces implementation of the fit() method.
    Inherit this class when creating a new model e.g. lpca.
    """

    # --- Class attributes ---
    INIT_METHODS = ["random", "kmeans", "uniform"]
    # --- Attributes ---
    def __init__(self):
        super().__init__()
        self.runtime = None
        # Algorithm settings
        self._verbose = True
        self._init_method = Model.INIT_METHODS[0]
        self.verboseprint = print if self.verbose else lambda *a, **k: None
        self.iter_max = 500
        self.conv_tol = 1e-8

    # --- Properties ---
    @property
    def init_method(self):
        return self._init_method

    @init_method.setter
    def init_method(self, value):
        self._init_method = check_method(value, INIT_METHODS)

    @property
    def verbose(self):
        return self._verbose

    @verbose.setter
    @accepts(object, bool)
    def verbose(self, value):
        self._verbose = value
        self.verboseprint = print if self.verbose else lambda *a, **k: None

    @property
    def iter_max(self):
        return self._iter_max

    @iter_max.setter
    @accepts(object, int)
    def iter_max(self, value):
        self._iter_max = value

    # --- Abstract methods ---
    @abstractmethod
    def fit(self):
        # subclasses will be able to call abstract methods as super().do_something()
        pass

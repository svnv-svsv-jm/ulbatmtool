__all__ = ["LPCA"]

import typing as ty
from loguru import logger

import torch
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.figure import Figure

from .utils import initialize_clusters, fit_lpca, pca_distance


class LPCA(torch.nn.Module):
    """Local PCA."""

    def __init__(
        self,
        n_clusters: int,
        q: int,
        conv_tol: float = 1e-6,
        iter_max: int = 100,
        device: ty.Union[str, torch.device] = "cpu",
        rel_tol: float = 1e-6,
        **kwargs: ty.Any,
    ) -> None:
        """_summary_
        Args:
            n_clusters (int):
                Number of clusters.
            q (int):
                Number of modes.
            conv_tol (float, optional): _description_. Defaults to 1e-6.
            iter_max (int, optional): _description_. Defaults to 100.
            device (ty.Union[str, torch.device], optional): _description_. Defaults to "cpu".
        """
        super().__init__(**kwargs)
        # Inputs
        self.n_clusters = n_clusters
        self.q = q
        self.iter_max = iter_max
        self.conv_tol = conv_tol
        self.device = torch.device(device) if isinstance(device, str) else device
        # Outputs
        self.centroids: torch.Tensor  # (k, variables)
        # One PCA solution per cluster
        self.A: torch.Tensor
        self.L: torch.Tensor
        self.Z: torch.Tensor
        self.C: torch.Tensor  # Sirovic's modes
        # Algorithm settings
        self.sirovich = False
        self.lpca_method = "false"
        self.residuals: torch.Tensor
        self.mu: torch.Tensor
        self.sig: torch.Tensor
        self.rel_tol = rel_tol

    def forward(self, X_new: torch.Tensor) -> torch.Tensor:
        """Classifies new observations and assigns them a cluster."""
        logger.debug("Classifying new observations...")
        # Pre-process new data
        X_new -= self.mu
        X_new /= self.sig
        # Check reconstruction error in each cluster
        sq_rec_err = pca_distance(X_new, self.centroids, self.A)
        # Assign the label
        idx = torch.argmin(sq_rec_err, dim=1)
        logger.debug("New observations classified.")
        return idx

    def fit(self, X: torch.Tensor) -> None:
        """Performs Local PCA."""
        logger.debug(f"Fitting Local PCA model with {self.n_clusters} clusters...")
        X = X.to(self.device)
        # Initialization
        convergence = False
        iteration = 0
        eps_rec = torch.tensor(1.0).to(self.device)
        residuals = torch.Tensor([0]).to(self.device)
        # Data pre-processing
        logger.debug("Centering and scaling the training matrix..")
        X0 = self.fit_scale(X)
        # Initialize solution
        idx = initialize_clusters(self.n_clusters, int(X0.size(0)))
        # Iterate
        while iteration < self.iter_max and not convergence:
            Z, L, A, centroids, sq_rec_err = fit_lpca(X=X0, idx=idx, q=self.q)
            assert not torch.isnan(sq_rec_err).any(), f"sq_rec_err={sq_rec_err}"
            # Update idx
            idx = torch.argmin(sq_rec_err, dim=0)
            assert X.size(0) == idx.size(0), f"{X.size()} but {idx.size()}"
            # Evaluate recovered variance
            rec_err_min, _ = sq_rec_err.min(1)
            eps_rec_new = rec_err_min.mean(0).view(-1)
            eps_rec_var = ((eps_rec_new - eps_rec) / (eps_rec_new + self.rel_tol)).abs()
            eps_rec = eps_rec_new.clone()
            # Print info
            logger.debug(f"Iteration: {iteration + 1}")
            logger.debug(f"\tReconstruction error: {eps_rec_new}")
            logger.debug(f"\tReconstruction error variance: {eps_rec_var}")
            assert not torch.isnan(eps_rec_new).any(), f"eps_rec_new={eps_rec_new}"
            # Check convergence
            if eps_rec_var <= self.conv_tol:
                convergence = True
            else:
                logger.trace(f"residuals: {residuals.size()}")
                logger.trace(f"eps_rec_new: {eps_rec_new.size()}")
                residuals = torch.cat((residuals, eps_rec_new), dim=0)
            # Update counter
            iteration += 1
        # Final steps
        logger.info(f"Convergence reached in {iteration} iterations.")
        self.A = A
        self.L = L
        self.Z = Z
        self.centroids = centroids
        self.residuals = residuals
        logger.debug("Local PCA model initialized!")

    def fit_scale(self, x: torch.Tensor) -> torch.Tensor:
        """Center scale data."""
        self.mu = x.mean(0, keepdim=True)
        self.sig = x.std(0, unbiased=False, keepdim=True)
        x -= self.mu
        x /= self.sig
        return x

    def recover(self, X: torch.Tensor) -> torch.Tensor:
        """Reconstruct the original matrix from the reduced PCA-manifold.
        Returns:
            X_rec (torch.Tensor): uncentered and unscaled reconstructed matrix with PCA modes.
        """
        X_rec = X * 0
        idx: torch.Tensor = self(X)
        cluster_ids = idx.unique()  # type: ignore
        for ii in range(0, self.A.size(0)):
            I = self.idx == cluster_ids[ii]
            rec = torch.matmul(self.Z[ii], self.A[ii].t()).view(-1)
            rec = rec + self.centroids[ii].view(-1)
            X_rec[I, :] = rec
        X_rec = X_rec * (self.sig + self.rel_tol)
        X_rec = X_rec + self.mu
        return X_rec

    def plot_residuals(
        self,
        font_size: int = 18,
        color: str = "b",
        marker: str = "s",
        linestyle: str = "-",
        linewidth: int = 2,
        markersize: int = 4,
        markerfacecolor: str = "b",
    ) -> Figure:
        """Plots residuals."""
        matplotlib.rcParams.update({"font.size": font_size, "text.usetex": True})
        n = int(self.residuals.size(0))
        itr = torch.linspace(1, n, n).numpy()
        fig: Figure = plt.figure()
        axes = fig.add_axes((0.15, 0.15, 0.7, 0.7), frameon=True)
        axes.plot(
            itr,
            self.residuals.cpu().numpy(),
            color=color,
            marker=marker,
            linestyle=linestyle,
            linewidth=linewidth,
            markersize=markersize,
            markerfacecolor=markerfacecolor,
        )
        axes.set_xlabel("Iterations [-]")
        axes.set_ylabel("Reconstruction error [-]")
        axes.set_title("Local PCA")
        return fig

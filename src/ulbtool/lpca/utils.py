__all__ = ["initialize_clusters", "fit_lpca", "pca_distance"]

from loguru import logger
import typing as ty
import torch

TENSOR = torch.Tensor


def initialize_clusters(k: int, n: int, method: str = "uniform") -> torch.Tensor:
    """Initialize vector of cluster assignements."""
    idx = torch.randint(0, k, (n,))
    if method.lower() in ["uniform", "uni"]:
        idx, _ = idx.sort()
    return idx


def get_centroids(X: TENSOR, idx: TENSOR = None) -> TENSOR:
    """_summary_
    Args:
        X (TENSOR): _description_
        idx (TENSOR, optional): _description_. Defaults to None.
    Returns:
        TENSOR: _description_
    """
    if idx is None:
        idx = torch.ones(X.size(0)).to(X.device).long()
    ids: TENSOR = idx.unique()  # type: ignore
    n_clusters = ids.view(-1).size(0)
    centroids = torch.zeros(n_clusters, X.size(1)).to(X.device)
    logger.trace(f"idx:{idx.size()} | X:{X.size()}")
    for cluster_id in ids:
        c = (X[idx == cluster_id]).mean(0)
        centroids[cluster_id, :] = c
    return centroids


def fit_lpca(X: TENSOR, idx: TENSOR, q: int = None) -> ty.Sequence[torch.Tensor]:
    """Given a cluster assignment solution vector `idx`, this runs PCA in each cluster.
    Args:
        X (torch.Tensor): (N,D)
            Input data.
        idx (torch.Tensor): (N,)
            Cluster assignments.
        q (int):
            Number of modes to keep.
    Returns:
        u (torch.Tensor): (K,N,D)
        s (torch.Tensor): (K,D)
        v (torch.Tensor): (K,D,D)
        centroids (torch.Tensor): (K,D)
            Matrix of `K` centroids of dimension `D`.
        sq_rec_err (torch.Tensor): (K,N)
            Vectors of distances. Element `sq_rec_err[k,n]` is the distance between `X[n,:]` and centroid `c[k,:]`.
    """
    # Get number of clusters
    K: int = len(idx.unique())  # type: ignore
    # Get centroids
    centroids = get_centroids(X, idx)  # (K,D)
    logger.trace(f"Centroids: {centroids.size()}")
    # Batch X of size (N, D) into (K, N, D)
    X_batched = X.unsqueeze(0).repeat((K, 1, 1))
    # We can now perform SVD on the unsqueezed X (K,N,D), to get:
    u: torch.Tensor  # (K,N,D)
    s: torch.Tensor  # (K,D)
    v: torch.Tensor  # (K,D,D)
    u, s, v = torch.svd(X_batched)
    # Select modes
    if q is not None:
        v = v[:, :, 0:q]
    # Estimate reconstruction errors
    sq_rec_err = pca_distance(X, centroids, v)  # (K,N)
    # Return
    return u, s, v, centroids, sq_rec_err


def pca_distance(
    X: torch.Tensor,
    c: torch.Tensor,
    A: torch.Tensor,
) -> torch.Tensor:
    """Distance of each row in `X` from the manifold `A` with centroid `c`.
    Args:
        X (torch.Tensor): (N,D)
            Input matrix.
        c (torch.Tensor): (K,D)
            Centroid vector.
        A (torch.Tensor): (K,D,q)
            PCA modes.
    Returns:
        sq_rec_err (torch.Tensor): # (K,N)
            Vectors of distances. Element `sq_rec_err[k,n]` is the distance between `X[n,:]` and centroid `c[k,:]`.
    """
    # Make sure dimensions are correct
    assert X.dim() == 2, f"X has size {X.size()} but expected to have `X.dim()==2`."
    assert A.dim() == 3, f"A has size {A.size()} but expected to have `A.dim()==3`."
    assert c.dim() == 2, f"c has size {c.size()} but expected to have `c.dim()==2`."
    # Debug
    logger.trace(f"X has size {X.size()}")
    logger.trace(f"A has size {A.size()}")
    logger.trace(f"c has size {c.size()}")
    # Ger relevant sizes
    N = X.size(0)  # number of observations
    D = X.size(-1)  # number of dimensions
    K = A.size(0)  # number of clusters
    # Sanity checks
    assert c.size(0) == K, f"Number of centroids {int(c.size(0))} not equal to number of clusters {K}."
    # Create matrix of centroids (K,N,D)
    c = c.unsqueeze(1)  # now it is (K,1,D)
    assert c.size() == (K, 1, D), f"{c.size()} not equal to {(K, 1 ,D)}"
    C_mat = c.repeat((1, N, 1))  # now it is (K,N,D)
    X_rep = X.unsqueeze(0).repeat((K, 1, 1))  # now it is (K,N,D)
    # Get distance of each observation from each centroid
    # X0[k,n,;] is the difference between centroids k and observation n
    X0 = X_rep - C_mat  # (K,N,D)
    # Project data onto modes to get scores
    Z = torch.matmul(X0, A)  # (K,N,q)
    logger.trace(f"Z: {Z.size()}")
    assert not torch.isnan(Z).any(), f"Z={Z}"
    # Recover original data
    X_rec = torch.matmul(Z, A.permute((0, 2, 1))) + C_mat  # (K,N,D)
    logger.trace(f"X_rec: {X_rec.size()}")
    assert not torch.isnan(X_rec).any(), f"X_rec={X_rec}"
    # Estimate errors
    rec_err_os = X_rep - X_rec  # (K,N,D)
    logger.trace(f"rec_err_os: {rec_err_os.size()}")
    assert rec_err_os.size() == (K, N, D), f"{rec_err_os.size()} but {(K, N, D)}"
    assert not torch.isnan(rec_err_os).any(), f"rec_err_os={rec_err_os}"
    # Square-root and sum errors
    sq_rec_err = rec_err_os.abs().sqrt().sum(-1)  # (K,N)
    logger.trace(f"sq_rec_err: {sq_rec_err.size()}")
    assert sq_rec_err.size() == (K, N), f"{sq_rec_err.size()} but {(K, N)}"
    return sq_rec_err  # (K,N)

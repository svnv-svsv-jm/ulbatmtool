__all__ = ["get_field"]

from loguru import logger
import typing as ty
import numpy as np


def get_field(
    choice: ty.Union[str, int],
    X: np.ndarray,
    mesh: np.ndarray,
    variables: ty.List[str],
) -> np.ndarray:
    """Given a snapshot matrix, this function returns the field(s) of the indicated variable ("choice" input argument).
    Args:
        choice (ty.Union[str, int]): variable whose field you want to select.
        X (np.ndarray): data matrix, each row is a set of vectorized spatial fields.
        mesh (np.ndarray): spatial mesh of the fields.
        variables (ty.List[str]): names of the variables.
    Returns:
        np.ndarray: subset of the matrix X: same number of rows, only selected columns.
    """
    logger.debug(f"Getting {choice} from {variables}...")
    # Check consistency of X
    assert (
        X.shape[1] == len(variables) * mesh.shape[0]
    ), f"Incosistent inputs: X.shape[1] = {X.shape[1]}; len(variables) = {len(variables)}; mesh.shape[0] = {mesh.shape[0]}."
    # Handle choice
    if isinstance(choice, str):
        if choice not in variables:
            raise ValueError("Choice has to be in variables.")
        # Find position of the variable
        for ii in range(len(variables)):
            if variables[ii] == choice:
                pos = ii
                break
    elif isinstance(choice, int):
        if choice < 0 or choice > len(variables):
            raise ValueError("Index out of bounds.")
        pos = choice  # position of the variable
        choice = variables[choice]
    # Get starting and ending column indeces
    start = mesh.shape[0] * pos
    end = mesh.shape[0] * (pos + 1)  # - 1 (end index is not included)
    # Do not go beyond the end
    if end > X.shape[1]:
        end = X.shape[1]
    # Return extracted fields
    field = X[:, start:end]
    assert field.shape[1] == mesh.shape[0]
    return X[:, start:end]
